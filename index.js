

let getNext = document.getElementById("next");
let buttons = Array.from(document.getElementsByClassName('button'));

buttons.map(button => {
  button.addEventListener('click', (event) =>{
    switch(event.target.innerText){
      case 'C':
        getNext.innerText = '';
        break;
      case 'r ':
        getNext.innerText = getNext.innerText.slice(0, -1);
        break;
      case '=':
        try{getNext.innerText= eval(getNext.innerText);}
        catch{getNext.innerText = 'Error';}
        break;
      default:   
        getNext.innerText+= event.target.innerText;
    }
  });
});